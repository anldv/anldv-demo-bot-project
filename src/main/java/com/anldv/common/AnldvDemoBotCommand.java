package com.anldv.common;

public class AnldvDemoBotCommand {
    public static final String MONEY_COMMAND        = "/money";
    public static final String SUBSCRIBE_COMMAND    = "/subscribe";
    public static final String UNSUBSCRIBE_COMMAND  = "/unsubscribe";
    public static final String VIEW_ORDER_COMMAND   = "/view_order";
}
