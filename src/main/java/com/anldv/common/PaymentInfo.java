package com.anldv.common;

import lombok.Data;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class PaymentInfo {

    //@Value("")
    public static final String BANK_TRANSFER = "\n - bank: VIETINBANK" +
            "\n - number: 102868956976" +
            "\n - user: LUU DINH VIET AN" +
            "\n - note: Sau khi chuyển tiền đến tài khoản chỉ định, hệ thống sẽ tự động cập nhật số dư tài khoản cho bạn.";
}
