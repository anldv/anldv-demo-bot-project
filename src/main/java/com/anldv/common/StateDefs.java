package com.anldv.common;

public class StateDefs {
    //state status
    public static final String NEW_USER_STATE           = "NEW_USER_STATE";
    public static final String SUBSCRIBED_STATE         = "SUBSCRIBED_STATE";
    public static final String SERVICE_SELECTED_STATE   = "SERVICE_SELECTED_STATE";
    public static final String ENTERED_API_KEY_STATE    = "ENTERED_API_KEY_STATE";
    public static final String ENTERED_SECRET_KEY_STATE = "ENTERED_SECRET_KEY_STATE";
    public static final String USING_SERVICE_STATE      = "USING_SERVICE_STATE";

    //state message
    public static final String SUBSCRIBE_FAILED             = "Đăng kí sử dụng dịch vụ thất bại. Vui lòng thử đăng ký lại dịch vụ";
    public static final String BALANCE_NOT_ENOUGH           = "Số dư tài khoản không đủ 50 cành để sử dụng dịch vụ này. Mời nạp thêm tiền và thử lại.";

    public static final String AFTER_SUBSCRIBE              = "Đã đăng ký sử dụng dịch vụ, mức phí dịch vụ là 50 cành, mời bạn lựa chọn dịch vụ Binance (nhập: \"BINANCE\") hoặc Ftx (nhập \"FTX\"):";
    public static final String AFTER_SERVICE_SELECTED       = "Đã lựa chọn dịch vụ, mời bạn nhập API key:";
    public static final String AFTER_ENTERED_API_KEY        = "Đã nhập API key, mời bạn nhập Secret key:";
    public static final String AFTER_ENTERED_SECRET_KEY     = "Đã nhập Secret key, đang kiểm tra tính hợp lệ của các key.";
    public static final String USER_USING_SERVICE           = "Bạn đang sử dụng dịch vụ.";
}
