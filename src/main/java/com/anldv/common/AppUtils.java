package com.anldv.common;

import com.github.slugify.Slugify;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppUtils {
    private static final char[] SOURCE_CHARACTERS = new char[]{'À', 'Á', 'Â', 'Ã', 'È', 'É', 'Ê', 'Ì', 'Í', 'Ò', 'Ó', 'Ô', 'Õ', 'Ù', 'Ú', 'Ý', 'à', 'á', 'â', 'ã', 'è', 'é', 'ê', 'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú', 'ý', 'Ă', 'ă', 'Đ', 'đ', 'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ạ', 'ạ', 'Ả', 'ả', 'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 'ậ', 'Ắ', 'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ', 'Ẻ', 'ẻ', 'Ẽ', 'ẽ', 'Ế', 'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ', 'ệ', 'Ỉ', 'ỉ', 'Ị', 'ị', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 'Ổ', 'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 'ỡ', 'Ợ', 'ợ', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ', 'Ử', 'ử', 'Ữ', 'ữ', 'Ự', 'ự'};
    private static final char[] DESTINATION_CHARACTERS = new char[]{'A', 'A', 'A', 'A', 'E', 'E', 'E', 'I', 'I', 'O', 'O', 'O', 'O', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'y', 'A', 'a', 'D', 'd', 'I', 'i', 'U', 'u', 'O', 'o', 'U', 'u', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'I', 'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u'};
    public static final String KEYPOINT_PATTERN = "\\{(.+?)\\}";
    public static final String EMAIL_ENCODING = "UTF-8";
    public static final SimpleDateFormat PART_FORMAT = new SimpleDateFormat("yyyyMMdd");
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final Random rand = new Random();
    private static final String USERNAME_PATTERN = "^[A-Za-z0-9_]{4,30}$";
    private static final String FULLNAME_PATTERN = "^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$";
    static String regexURL = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    static final String regexEmail = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

    public AppUtils() {
    }

    public static int randInt(int min, int max) {
        return rand.nextInt(max - min + 1) + min;
    }

    public static Date stringToDate(String date, String pattern) {
        if (Strings.isNullOrEmpty(date)) {
            return new Date();
        } else {
            Date date1;
            try {
                date1 = (new SimpleDateFormat(pattern)).parse(date);
            } catch (Exception var4) {
                date1 = new Date();
            }

            return date1;
        }
    }

    public static char removeAccent(char ch) {
        int index = Arrays.binarySearch(SOURCE_CHARACTERS, ch);
        if (index >= 0) {
            ch = DESTINATION_CHARACTERS[index];
        }

        return ch;
    }

    public static String removeAccent(String s) {
        StringBuilder sb = new StringBuilder(s);

        for(int i = 0; i < sb.length(); ++i) {
            sb.setCharAt(i, removeAccent(sb.charAt(i)));
        }

        return sb.toString();
    }

    public static String slugify(String name) {
        Slugify slugify = new Slugify();
        return slugify.slugify(removeAccent(name)) + (new Random()).nextInt(10000);
    }

    public static Date stringToDate2(String date, String pattern) {
        if (Strings.isNullOrEmpty(date)) {
            return null;
        } else {
            Date date1;
            try {
                date1 = (new SimpleDateFormat(pattern)).parse(date);
            } catch (Exception var4) {
                date1 = null;
            }

            return date1;
        }
    }

    public static int formatDate(Date date) {
        return formatDate(date, "yyyyMMdd");
    }

    public static int formatDate(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return parseInt(simpleDateFormat.format(date));
    }

    public static String formatDateToString(Date date) {
        return formatDateToString(date, "dd/MM/yyyy HH:mm:ss");
    }

    public static String formatDateToString(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static String parseString(Object obj) {
        if (obj == null) {
            return "";
        } else {
            try {
                return String.valueOf(obj);
            } catch (Exception var2) {
                return "";
            }
        }
    }

    public static long parseLong(Object o) {
        if (o == null) {
            return 0L;
        } else if (o instanceof Double) {
            return ((Double)o).longValue();
        } else if (o instanceof Float) {
            return ((Float)o).longValue();
        } else {
            try {
                return Long.parseLong(String.valueOf(o));
            } catch (Exception var2) {
                return 0L;
            }
        }
    }

    public static int parseInt(Object o) {
        if (o == null) {
            return 0;
        } else if (o instanceof Double) {
            return ((Double)o).intValue();
        } else if (o instanceof Float) {
            return ((Float)o).intValue();
        } else {
            try {
                return Integer.parseInt(String.valueOf(o));
            } catch (Exception var2) {
                return 0;
            }
        }
    }

    public static double parseDouble(Object o) {
        if (o == null) {
            return 0.0D;
        } else {
            try {
                return Double.parseDouble(String.valueOf(o));
            } catch (Exception var2) {
                return 0.0D;
            }
        }
    }

    public static String MD5(String md5) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuilder sb = new StringBuilder();
            byte[] var4 = array;
            int var5 = array.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                byte b = var4[var6];
                sb.append(Integer.toHexString(b & 255 | 256), 1, 3);
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException var8) {
            return null;
        }
    }

    public static boolean validateUsername(final String username) {
        if (Strings.isNullOrEmpty(username)) {
            return false;
        } else {
            Pattern pattern = Pattern.compile("^[A-Za-z0-9_]{4,30}$");
            Matcher matcher = pattern.matcher(username);
            return matcher.matches();
        }
    }

    public static boolean validateFullName(String fullName) {
        return Strings.isNullOrEmpty(fullName) ? false : fullName.matches("^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$");
    }

    public static boolean validatePhone(String phone) {
        String regex = "(09|03|05|08|07)[0-9]{8}";
        return Strings.isNullOrEmpty(phone) ? false : phone.matches(regex);
    }

    public static boolean validateUrl(String url) {
        if (Strings.isNullOrEmpty(url)) {
            return false;
        } else {
            Pattern pattern = Pattern.compile(regexURL);
            Matcher matcher = pattern.matcher(url);
            return matcher.matches();
        }
    }

    public static boolean validateEmail(String email) {
        if (Strings.isNullOrEmpty(email)) {
            return false;
        } else {
            Pattern pattern = Pattern.compile("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        }
    }

    public static String parseVietnameseToEnglish(String str) {
        if (Strings.isNullOrEmpty(str)) {
            return "";
        } else {
            String temp = Normalizer.normalize(str, Normalizer.Form.NFD);
            Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
            return pattern.matcher(temp).replaceAll("").replaceAll(" +", " ").trim().toLowerCase().replaceAll("đ", "d");
        }
    }

    public static String mergeWhitespace(String str) {
        return Strings.isNullOrEmpty(str) ? "" : str.trim().replaceAll(" +", " ");
    }

    public static double roundDouble(Double d) {
        return d == null ? 0.0D : (double)Math.round(d * 100.0D) / 100.0D;
    }

    public static String saveFile(String serverPath, MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        File file1 = new File(serverPath + fileName);

        for(int i = 0; file1.exists(); file1 = new File(serverPath + fileName)) {
            ++i;
            String[] arr = ((String) Objects.requireNonNull(file.getOriginalFilename())).split("\\.");
            fileName = arr[0] + " (" + i + ")." + arr[1];
        }

        FileOutputStream fos = new FileOutputStream(file1);
        fos.write(file.getBytes());
        fos.close();
        return serverPath + fileName;
    }
}

