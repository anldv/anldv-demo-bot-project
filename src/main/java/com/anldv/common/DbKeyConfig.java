package com.anldv.common;

public class DbKeyConfig {
    //discord
    public static final String DISCORD_ID       = "discordId";
    public static final String DISCORD_NAME     = "discordName";
    public static final String SERVICE_SELECTED = "serviceSelected";
    public static final String USER_STATE       = "userState";
    public static final String API_KEY          = "apiKey";
    public static final String SECRET_KEY        = "secretKey";
    public static final String BALANCE          = "balance";
    public static final String CREATE_AT        = "createAt";
    public static final String UPDATE_AT        = "updateAt";

    //telegram
}
