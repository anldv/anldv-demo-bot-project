package com.anldv.domain.entity.cex.svc;

import com.anldv.common.CexType;

public class CexSvcFactory {
    public CexSvc getCexSvc(String cexType) {
        if (cexType.equals(CexType.BINANCE_SVC)) {
            return new BinanceSvc();
        }
        if (cexType.equals(CexType.FTX_SVC)) {
            return new FtxSvc();
        }
        return null;
    }
}
