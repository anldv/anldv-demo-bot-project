package com.anldv.domain.entity.cex.svc;

import com.anldv.common.CexType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BinanceSvc implements CexSvc {
    private String serviceName = CexType.BINANCE_SVC;
    private String ApiKey;
    private String SecretKey;

    @Override
    public String getServiceName() {
        return this.serviceName;
    }
}
