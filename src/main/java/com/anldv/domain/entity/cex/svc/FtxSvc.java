package com.anldv.domain.entity.cex.svc;

import com.anldv.common.CexType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FtxSvc implements CexSvc {
    private String serviceName = CexType.FTX_SVC;
    private String ApiKey;
    private String SecretKey;

    @Override
    public String getServiceName() {
        return this.serviceName;
    }
}
