package com.anldv.domain.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiscordUser {
    private long discordId;
    private String discordName;
    private String serviceSelected;
    private String userState;
    private String apiKey;
    private String secretKey;
    private long balance;
    private long createAt;
    private long updateAt;

    public DiscordUser(long discordId, String discordName) {
        this.discordId = discordId;
        this.discordName = discordName;
    }
}
