package com.anldv.domain.entity.bot;

import com.anldv.common.StateDefs;
import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.domain.state.*;
import com.anldv.service.DiscordUserService;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.stereotype.Component;

@Component
public class DiscordTradingBot extends ListenerAdapter {

    private String botToken = "OTczNzY2OTIxNTg3NjYyODg4.GwfH6q.iv53AnJHgAlBHZ5VBraSrlMmSwsdjytxKBvKG4";

    private final DiscordUserService discordUserService;
    private final SubscribedState subscribedState;
    private final ServiceSelectedState serviceSelectedState;
    private final EnteredApiKeyState enteredApiKeyState;
    private final WaitingNewCommandState waitingNewCommandState;

    public DiscordTradingBot(
            DiscordUserService discordUserService,
            SubscribedState subscribedState,
            ServiceSelectedState serviceSelectedState,
            EnteredApiKeyState enteredApiKeyState,
            WaitingNewCommandState waitingNewCommandState) {
        this.discordUserService = discordUserService;
        this.subscribedState = subscribedState;
        this.serviceSelectedState = serviceSelectedState;
        this.enteredApiKeyState = enteredApiKeyState;
        this.waitingNewCommandState = waitingNewCommandState;
    }

    public String getBotToken() {
        return this.botToken;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        long senderId           = event.getMessage().getAuthor().getIdLong();
        String senderName       = event.getMessage().getAuthor().getName();
        DiscordUser user        = discordUserService
                .getOrCreateDiscordUser(new DiscordUser(senderId, senderName));
        String state            = user.getUserState();
        UserState userState     = null;

        if (state.equals(StateDefs.SUBSCRIBED_STATE)) {
            userState = subscribedState;
        }
        if (state.equals(StateDefs.SERVICE_SELECTED_STATE)) {
            userState = serviceSelectedState;
        }
        if (state.equals(StateDefs.ENTERED_API_KEY_STATE)) {
            userState = enteredApiKeyState;
        }
        if (userState == null) {
            userState = waitingNewCommandState;
        }

        if (userState != null) {
            userState.handleRequest(event);
        }

    }

}
