package com.anldv.domain.entity.bot;

import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.domain.entity.user.User;

public interface Bot {

    String money(DiscordUser user);

    String subscribe(DiscordUser user);

    String unsubscribe(DiscordUser user);

    String viewOrder(DiscordUser user);

    String invalid(DiscordUser user);
}
