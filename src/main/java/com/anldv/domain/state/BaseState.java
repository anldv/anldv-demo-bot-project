package com.anldv.domain.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseState implements UserState {
    protected final Logger logger = LoggerFactory.getLogger(getClass());
}
