package com.anldv.domain.state;

import com.anldv.command.new_cmd.*;
import com.anldv.common.AnldvDemoBotCommand;
import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.service.DiscordUserService;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

@Component
public class WaitingNewCommandState extends BaseState {

    private final MoneyCommand moneyCommand;
    private final SubScribeCommand subScribeCommand;
    private final UnsubscribeCommand unsubscribeCommand;
    private final ViewOrderCommand viewOrderCommand;

    public WaitingNewCommandState(MoneyCommand moneyCommand, SubScribeCommand subScribeCommand, UnsubscribeCommand unsubscribeCommand, ViewOrderCommand viewOrderCommand, DiscordUserService discordUserService) {
        this.moneyCommand = moneyCommand;
        this.subScribeCommand = subScribeCommand;
        this.unsubscribeCommand = unsubscribeCommand;
        this.viewOrderCommand = viewOrderCommand;
    }

    public void handleRequest(MessageReceivedEvent event) {
        ICommand cmd = null;
        String command = event.getMessage().getContentRaw();
        logger.info("<=WaitingNewCommandState event : {}", event.getMessage().getContentRaw());

        if (command.equals(AnldvDemoBotCommand.SUBSCRIBE_COMMAND)) {
            cmd = subScribeCommand;
        }
        if (command.equals(AnldvDemoBotCommand.MONEY_COMMAND)) {
            cmd = moneyCommand;
        }
        if (command.equals(AnldvDemoBotCommand.UNSUBSCRIBE_COMMAND)) {
            cmd = unsubscribeCommand;
        }
        if (command.equals(AnldvDemoBotCommand.VIEW_ORDER_COMMAND)) {
            cmd = viewOrderCommand;
        }

        if (cmd != null) {
            cmd.doAction(event);
        }
    }

}
