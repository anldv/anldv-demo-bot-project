package com.anldv.domain.state;

import com.anldv.common.StateDefs;
import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.service.DiscordUserService;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EnteredSecretKeyState implements UserState {

    @Autowired
    DiscordUserService discordUserService;

    public void handleRequest(MessageReceivedEvent event) {

    }
}
