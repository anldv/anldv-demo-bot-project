package com.anldv.domain.state;

import com.anldv.domain.entity.user.DiscordUser;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public interface UserState {
    void handleRequest(MessageReceivedEvent event);
}
