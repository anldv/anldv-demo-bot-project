package com.anldv.domain.state;

import com.anldv.common.StateDefs;
import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.service.DiscordUserService;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EnteredApiKeyState extends BaseState {

    @Autowired
    DiscordUserService discordUserService;

    @Override
    public void handleRequest(MessageReceivedEvent event) {
        logger.info("<=EnteredApiKeyState event : {}", event.getMessage().getContentRaw());
        MessageChannel messageChannel = event.getChannel();
        long senderId = event.getMessage().getAuthor().getIdLong();
        String senderName = event.getMessage().getAuthor().getName();
        DiscordUser user = discordUserService.getOrCreateDiscordUser(new DiscordUser(senderId, senderName));

        String response;
        String secretKey = event.getMessage().getContentRaw();
        user.setSecretKey(secretKey);
        user.setUserState(StateDefs.ENTERED_SECRET_KEY_STATE);
        discordUserService.updateDiscordUser(user);
        response = StateDefs.AFTER_ENTERED_SECRET_KEY;
        messageChannel.sendMessage(response).queue();
        logger.info("=>EnteredApiKeyState event : {}, resp : {}", event.getMessage().getContentRaw(), response);
    }
}
