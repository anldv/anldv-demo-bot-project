package com.anldv.domain.state;

import com.anldv.domain.entity.user.DiscordUser;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class UserContext {
    private UserState userState;

    public void setUserState(UserState userState) {
        this.userState = userState;
    }

    public void applyState(MessageReceivedEvent event) {
        this.userState.handleRequest(event);
    }
}
