package com.anldv.domain.state;

import com.anldv.common.StateDefs;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class StateFactory {

    private final NewUserState newUserState;
    private final SubscribedState subscribedState;
    private final ServiceSelectedState serviceSelectedState;
    private final EnteredApiKeyState enteredApiKeyState;
    private final EnteredSecretKeyState enteredSecretKeyState;

    public StateFactory(@Lazy NewUserState newUserState,
                        @Lazy SubscribedState subscribedState,
                        @Lazy ServiceSelectedState serviceSelectedState,
                        @Lazy EnteredApiKeyState enteredApiKeyState,
                        @Lazy EnteredSecretKeyState enteredSecretKeyState) {
        this.newUserState = newUserState;
        this.subscribedState = subscribedState;
        this.serviceSelectedState = serviceSelectedState;
        this.enteredApiKeyState = enteredApiKeyState;
        this.enteredSecretKeyState = enteredSecretKeyState;
    }


    public UserState getUserState(String userState) {
        if (userState.equals(StateDefs.NEW_USER_STATE)) {
            return newUserState;
        }
        if (userState.equals(StateDefs.SUBSCRIBED_STATE)) {
            return subscribedState;
        }
        if (userState.equals(StateDefs.SERVICE_SELECTED_STATE)) {
            return serviceSelectedState;
        }
        if (userState.equals(StateDefs.ENTERED_API_KEY_STATE)) {
            return enteredApiKeyState;
        }
        if (userState.equals(StateDefs.ENTERED_SECRET_KEY_STATE)) {
            return enteredSecretKeyState;
        }
        return null;
    }
}
