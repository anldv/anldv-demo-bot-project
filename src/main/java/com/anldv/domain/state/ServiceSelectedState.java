package com.anldv.domain.state;

import com.anldv.common.StateDefs;
import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.service.DiscordUserService;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceSelectedState extends BaseState {

    @Autowired
    DiscordUserService discordUserService;

    public void handleRequest(MessageReceivedEvent event) {
        logger.info("<=ServiceSelectedState event : {}", event.getMessage().getContentRaw());
        MessageChannel messageChannel = event.getChannel();
        long senderId = event.getMessage().getAuthor().getIdLong();
        String senderName = event.getMessage().getAuthor().getName();
        DiscordUser user = discordUserService.getOrCreateDiscordUser(new DiscordUser(senderId, senderName));

        String response;
        String apiKey = event.getMessage().getContentRaw();
        user.setApiKey(apiKey);
        user.setUserState(StateDefs.ENTERED_API_KEY_STATE);
        discordUserService.updateDiscordUser(user);
        response = StateDefs.AFTER_ENTERED_API_KEY;
        messageChannel.sendMessage(response).queue();
        logger.info("=>ServiceSelectedState event : {}, resp : {}", event.getMessage().getContentRaw(), response);
    }
}
