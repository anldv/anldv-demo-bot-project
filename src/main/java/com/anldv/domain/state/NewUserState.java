package com.anldv.domain.state;

import com.anldv.service.DiscordUserService;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewUserState implements UserState {

    @Autowired
    DiscordUserService discordUserService;

    public void handleRequest(MessageReceivedEvent event) {

    }
}
