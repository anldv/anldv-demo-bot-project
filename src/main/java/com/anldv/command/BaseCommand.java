package com.anldv.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseCommand {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
}
