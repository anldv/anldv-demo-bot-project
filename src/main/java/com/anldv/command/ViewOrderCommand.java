package com.anldv.command;

import com.anldv.domain.entity.bot.Bot;
import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.domain.entity.user.User;

//Xem danh sách order
public class ViewOrderCommand extends BaseCommand implements Command {

    private Bot bot;

    public ViewOrderCommand(Bot bot) {
        this.bot = bot;
    }

    @Override
    public String execute(DiscordUser user) {
        this.logger.info("=>viewOrderCommand");
        String response = bot.viewOrder(user);
        return response;
    }
}
