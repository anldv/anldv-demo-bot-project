package com.anldv.command;

import com.anldv.domain.entity.bot.Bot;
import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.domain.entity.user.User;

public class InvalidCommand extends BaseCommand implements Command {

    private Bot bot;

    public InvalidCommand(Bot bot) {
        this.bot = bot;
    }

    @Override
    public String execute(DiscordUser user) {
        this.logger.info("=>invalidCommand");
        String response = this.bot.invalid(user);
        return response;
    }
}
