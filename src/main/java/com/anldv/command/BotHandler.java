package com.anldv.command;

import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.domain.entity.user.User;

public class BotHandler {
    private Command moneyCommand;
    private Command subscribeCommand;
    private Command unsubscribeCommand;
    private Command viewOrderCommand;
    private Command invalidCommand;

    public BotHandler(Command moneyCommand, Command subscribeCommand, Command unsubscribeCommand, Command viewOrderCommand, Command invalidCommand) {
        this.moneyCommand = moneyCommand;
        this.subscribeCommand = subscribeCommand;
        this.unsubscribeCommand = unsubscribeCommand;
        this.viewOrderCommand = viewOrderCommand;
        this.invalidCommand = invalidCommand;
    }

    public String sendMoneyCommand(DiscordUser user) {
        return this.moneyCommand.execute(user);
    }

    public String sendSubscribeCommand(DiscordUser user) {
        return this.subscribeCommand.execute(user);
    }

    public String sendUnsubscribeCommand(DiscordUser user) {
        return this.unsubscribeCommand.execute(user);
    }

    public String sendViewOrderCommand(DiscordUser user) {
        return this.viewOrderCommand.execute(user);
    }

    public String sendInvalidCommand(DiscordUser user) {
        return this.invalidCommand.execute(user);
    }
}
