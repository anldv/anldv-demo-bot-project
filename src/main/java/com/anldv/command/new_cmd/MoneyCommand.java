package com.anldv.command.new_cmd;

import com.anldv.common.PaymentInfo;
import com.anldv.common.StateDefs;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Service;

@Service
public class MoneyCommand extends BaseCommand {
    @Override
    public void doAction(MessageReceivedEvent event) {
        logger.info("<=MoneyCommand event : {}", event.getMessage().getContentRaw());
        String response = PaymentInfo.BANK_TRANSFER;
        MessageChannel messageChannel = event.getChannel();
        messageChannel.sendMessage(response).queue();
        logger.info("=>MoneyCommand event : {}, resp : {}", event.getMessage().getContentRaw(), response);
    }
}
