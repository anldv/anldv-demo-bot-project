package com.anldv.command.new_cmd;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public interface ICommand {
    void doAction(MessageReceivedEvent event);
}
