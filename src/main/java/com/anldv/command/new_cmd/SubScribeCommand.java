package com.anldv.command.new_cmd;

import com.anldv.common.StateDefs;
import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.service.DiscordUserService;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SubScribeCommand extends BaseCommand {

    //@Value("${discord-service-price}")
    long discordServicePrice = 50000;

    @Autowired
    DiscordUserService discordUserService;

    @Override
    public void doAction(MessageReceivedEvent event) {
        logger.info("<=SubscribeCommand event : {}", event.getMessage().getContentRaw());
        MessageChannel messageChannel = event.getChannel();
        long senderId = event.getMessage().getAuthor().getIdLong();
        String senderName = event.getMessage().getAuthor().getName();
        DiscordUser user = discordUserService.getOrCreateDiscordUser(new DiscordUser(senderId, senderName));

        String response;
        if (user.getBalance() < discordServicePrice) {
            response = StateDefs.BALANCE_NOT_ENOUGH;
            logger.info("=>SubscribeCommand event: {}, resp : {}", event.getMessage().getContentRaw(), response);
            messageChannel.sendMessage(response).queue();
        }

        //Trừ tiền sau khi đăng kí dịch vụ và cập nhật trạng thái người dùng.
        //Tạm bỏ qua đoạn trừ tiền
        user.setUserState(StateDefs.SUBSCRIBED_STATE);
        discordUserService.updateDiscordUser(user);
        response = StateDefs.AFTER_SUBSCRIBE;
        messageChannel.sendMessage(response).queue();
        logger.info("=>MoneyCommand event : {}, resp : {}", event.getMessage().getContentRaw(), response);
    }

}
