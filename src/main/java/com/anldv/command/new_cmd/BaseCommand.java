package com.anldv.command.new_cmd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseCommand implements ICommand {
    protected final Logger logger = LoggerFactory.getLogger(getClass());
}
