package com.anldv.command;

import com.anldv.domain.entity.bot.Bot;
import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.domain.entity.user.User;

//Đăng ký sử dụng bot
public class SubscribeCommand extends BaseCommand implements Command {

    private Bot bot;

    public SubscribeCommand(Bot bot) {
        this.bot = bot;
    }

    @Override
    public String execute(DiscordUser user) {
        this.logger.info("=>subscribeCommand");
        String response = this.bot.subscribe(user);
        return response;
    }
}
