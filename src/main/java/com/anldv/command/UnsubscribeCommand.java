package com.anldv.command;

import com.anldv.domain.entity.bot.Bot;
import com.anldv.domain.entity.user.DiscordUser;

//Hủy sử dụng bot
public class UnsubscribeCommand extends BaseCommand implements Command {

    private Bot bot;

    public UnsubscribeCommand(Bot bot) {
        this.bot = bot;
    }

    @Override
    public String execute(DiscordUser user) {
        this.logger.info("=>unsubscribeCommand");
        String response = this.bot.unsubscribe(user);
        return response;
    }
}
