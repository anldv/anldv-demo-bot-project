package com.anldv.command;

import com.anldv.domain.entity.bot.Bot;
import com.anldv.domain.entity.user.DiscordUser;
import com.anldv.domain.entity.user.User;

//Nạp tiền 50$/tháng
public class MoneyCommand extends BaseCommand implements Command {

    private Bot bot;

    public MoneyCommand(Bot bot) {
        this.bot = bot;
    }

    @Override
    public String execute(DiscordUser user) {
        this.logger.info("=>moneyCommand");
        String response = this.bot.money(user);
        return response;
    }
}
