package com.anldv.command;

import com.anldv.domain.entity.user.DiscordUser;

public interface Command {

    String execute(DiscordUser user);

}
