package com.anldv;

import com.anldv.domain.entity.bot.DiscordTradingBot;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Test implements CommandLineRunner {

    private final DiscordTradingBot bot;

    public Test(DiscordTradingBot bot) {
        this.bot = bot;
    }

    @Override
    public void run(String... args) throws Exception {
        //discord bot
        try {
            JDA jda = JDABuilder
                    .createDefault(bot.getBotToken())
                    .build();
            jda.addEventListener(bot);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
