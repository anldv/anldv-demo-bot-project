package com.anldv;

import com.anldv.domain.entity.bot.DiscordTradingBot;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

import javax.annotation.PostConstruct;

@SpringBootApplication(exclude = MongoAutoConfiguration.class)
public class Main {
	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
	}

}
