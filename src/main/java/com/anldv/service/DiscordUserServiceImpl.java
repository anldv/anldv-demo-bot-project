package com.anldv.service;

import com.anldv.common.AppUtils;
import com.anldv.common.CollectionNameDefs;
import com.anldv.common.DbKeyConfig;
import com.anldv.common.StateDefs;
import com.anldv.domain.db.MongoDbOnlineSyncActions;
import com.anldv.domain.entity.user.DiscordUser;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.stereotype.Service;

@Service
public class DiscordUserServiceImpl extends BaseService implements DiscordUserService {

    private final MongoDbOnlineSyncActions db;

    public DiscordUserServiceImpl(MongoDbOnlineSyncActions db) {
        this.db = db;
    }

    //@Value("${binance_service_price}")
    private long servicePrice = 50000;

    @Override
    public DiscordUser saveUser(DiscordUser user) {
        return null;
    }

    @Override
    public DiscordUser createUserIfNotExist(long discordId, String discordName) {
        String serviceRegister  = "";
        String userState        = StateDefs.NEW_USER_STATE;
        String apiKey           = "";
        String secretKey        = "";
        long balance            = 50000;
        long createAt           = System.currentTimeMillis();
        long updateAt           = System.currentTimeMillis();
        DiscordUser discordUser = new DiscordUser(discordId, discordName, serviceRegister, userState, apiKey, secretKey, balance, createAt, updateAt);
        return createDiscordUser(discordUser);
    }

    @Override
    public DiscordUser saveUserStatus(DiscordUser user) {
        return null;
    }

    @Override
    public boolean isBalanceEnough (DiscordUser user) {
        long balance = getBalanceByDiscordUserId(user.getDiscordId());
        if (balance >= servicePrice) {
            return true;
        }
        return false;
    }

    @Override
    public Long getBalanceByDiscordUserId(long discordId) {
        long balance = 0;
        Bson userCond = Filters.eq(DbKeyConfig.DISCORD_ID, discordId);
        Document userDoc =db.findOne(CollectionNameDefs.COLL_DISCORD_USER, userCond);
        if (userDoc == null) {
            return null;
        }
        balance = AppUtils.parseLong(userDoc.get(DbKeyConfig.BALANCE));
        return balance;
    }

    @Override
    public DiscordUser getOrCreateDiscordUser(DiscordUser user) {
        DiscordUser discordUser = getUserByDiscordId(user.getDiscordId());
        if (discordUser == null) {
            discordUser =
                    createUserIfNotExist(user.getDiscordId(), user.getDiscordName());
        }
        return discordUser;
    }

    @Override
    public DiscordUser getUserByDiscordUsername(String username) {
        return null;
    }

    @Override
    public DiscordUser getUserByDiscordId(long id) {
        long discordId = id;
        Bson userCond = Filters.eq(DbKeyConfig.DISCORD_ID, discordId);
        Document userDoc = db
                .findOne(CollectionNameDefs.COLL_DISCORD_USER, userCond);
        if (userDoc == null) {
            return null;
        }
        DiscordUser discordUser = new DiscordUser();
        discordUser.setDiscordId(AppUtils.parseLong(userDoc.get(DbKeyConfig.DISCORD_ID)));
        discordUser.setDiscordName(AppUtils.parseString(userDoc.get(DbKeyConfig.DISCORD_NAME)));
        discordUser.setServiceSelected(AppUtils.parseString(userDoc.get(DbKeyConfig.SERVICE_SELECTED)));
        discordUser.setUserState(AppUtils.parseString(userDoc.get(DbKeyConfig.USER_STATE)));
        discordUser.setApiKey(AppUtils.parseString(userDoc.get(DbKeyConfig.API_KEY)));
        discordUser.setSecretKey(AppUtils.parseString(userDoc.get(DbKeyConfig.SECRET_KEY)));
        discordUser.setBalance(AppUtils.parseLong(userDoc.get(DbKeyConfig.BALANCE)));
        discordUser.setCreateAt(AppUtils.parseLong(userDoc.get(DbKeyConfig.CREATE_AT)));
        discordUser.setUpdateAt(AppUtils.parseLong(userDoc.get(DbKeyConfig.UPDATE_AT)));
        return discordUser;
    }

    @Override
    public DiscordUser createDiscordUser(DiscordUser discordUser) {
        long discordId = discordUser.getDiscordId();
        Bson userCond = Filters.eq(DbKeyConfig.DISCORD_ID, discordId);
        Document userDoc = db
                .findOne(CollectionNameDefs.COLL_DISCORD_USER, userCond);

        //handle nếu user đã tồn tại
        if (userDoc != null) {
        }

        Document user = new Document();
        user.append(DbKeyConfig.DISCORD_ID, discordUser.getDiscordId());
        user.append(DbKeyConfig.DISCORD_NAME, discordUser.getDiscordName());
        user.append(DbKeyConfig.SERVICE_SELECTED, discordUser.getServiceSelected());
        user.append(DbKeyConfig.USER_STATE, discordUser.getUserState());
        user.append(DbKeyConfig.API_KEY, discordUser.getApiKey());
        user.append(DbKeyConfig.SECRET_KEY, discordUser.getSecretKey());
        user.append(DbKeyConfig.BALANCE, discordUser.getBalance());
        user.append(DbKeyConfig.CREATE_AT, discordUser.getCreateAt());
        user.append(DbKeyConfig.UPDATE_AT, discordUser.getUpdateAt());

        db.insertOne(CollectionNameDefs.COLL_DISCORD_USER, user);

        return discordUser;
    }

    @Override
    public DiscordUser updateDiscordUser(DiscordUser user) {
        Bson userCond = Filters.eq(DbKeyConfig.DISCORD_ID, user.getDiscordId());
        Document userDoc = db.findOne(CollectionNameDefs.COLL_DISCORD_USER, userCond);

        //handle nếu user không tồn tại
        if (userDoc == null) {
        }

        Bson userUpdates;
        userUpdates = Updates.combine(
            Updates.set(DbKeyConfig.DISCORD_ID, user.getDiscordId()),
            Updates.set(DbKeyConfig.DISCORD_NAME, user.getDiscordName()),
            Updates.set(DbKeyConfig.SERVICE_SELECTED, user.getServiceSelected()),
            Updates.set(DbKeyConfig.USER_STATE, user.getUserState()),
            Updates.set(DbKeyConfig.API_KEY, user.getApiKey()),
            Updates.set(DbKeyConfig.SECRET_KEY, user.getSecretKey()),
            Updates.set(DbKeyConfig.BALANCE, user.getBalance()),
            Updates.set(DbKeyConfig.UPDATE_AT, System.currentTimeMillis())
        );
        db.update(CollectionNameDefs.COLL_DISCORD_USER, userCond, userUpdates);

        return user;
    }

    @Override
    public void deleteUserByDiscordId(String id) {

    }
}
