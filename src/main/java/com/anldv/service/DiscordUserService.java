package com.anldv.service;

import com.anldv.domain.entity.user.DiscordUser;

public interface DiscordUserService {

    DiscordUser saveUser(DiscordUser user);

    DiscordUser createUserIfNotExist(long discordId, String discordName);

    DiscordUser saveUserStatus(DiscordUser user);

    boolean isBalanceEnough(DiscordUser user);

    Long getBalanceByDiscordUserId(long discordId);

    DiscordUser getOrCreateDiscordUser(DiscordUser user);

    DiscordUser getUserByDiscordUsername(String username);

    DiscordUser getUserByDiscordId(long id);

    DiscordUser createDiscordUser(DiscordUser user);

    DiscordUser updateDiscordUser(DiscordUser user);

    void deleteUserByDiscordId(String id);
}
